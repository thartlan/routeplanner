package main

import (
	"net"

	"github.com/pkg/errors"
	"github.com/vishvananda/netlink"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/client-go/kubernetes"
	"k8s.io/klog"
)

// ensureRoute finds the node/pod IP of a ready tunnel pod and sets a route to
// the given remote subnet via this pod, on the host network.
func ensureRoute(clientset *kubernetes.Clientset, localNodeName string, remoteCIDR net.IPNet, podNamespace string, podSelector map[string]string) error {
	found, podIP, hostIP, nodeName, err := findPod(clientset, podNamespace, podSelector)
	if err != nil {
		return errors.Wrap(err, "could not find pod")
	}

	// If a pod is not found, it is not considered an error.
	if !found {
		klog.Warning("No ready pods found")
		return nil
	}

	klog.V(3).Infof("Found a ready pod, podIP=%s, hostIP=%s, nodeName=%s", podIP, hostIP, nodeName)

	currentRouteGw, foundRoute, err := currentRoute(remoteCIDR)
	if err != nil {
		return errors.Wrap(err, "Could not get current route")
	}

	var route netlink.Route
	if nodeName == localNodeName {
		// If the tunnel pod is on the same node, route directly to the pod IP.
		route = netlink.Route{
			Dst: &remoteCIDR,
			Gw:  net.ParseIP(podIP),
		}
	} else {
		// Else, route to the node IP via interface eth0.
		// "onlink" is required in case there is no direct
		// route to the other node.
		link, err := netlink.LinkByName("eth0")
		if err != nil {
			return errors.Wrap(err, "no eth0 interface")
		}
		linkIndex := link.Attrs().Index
		route = netlink.Route{
			Dst:       &remoteCIDR,
			Gw:        net.ParseIP(hostIP),
			LinkIndex: linkIndex,
			Flags:     int(netlink.FLAG_ONLINK),
		}
	}

	if !foundRoute || !route.Gw.Equal(currentRouteGw) {
		klog.V(0).Infof("Setting new route: %s", route.String())
	}

	err = netlink.RouteReplace(&route)
	if err != nil {
		return errors.Wrap(err, "could not set route")
	}

	return nil
}

// findPod finds the first ready pod matching the input label selectors in a given namespace.
func findPod(clientset *kubernetes.Clientset, namespace string, podSelector map[string]string) (bool, string, string, string, error) {
	podsClient := clientset.CoreV1().Pods(namespace)

	selector := labels.SelectorFromSet(labels.Set(podSelector))
	listOpts := metav1.ListOptions{LabelSelector: selector.String()}
	pods, err := podsClient.List(listOpts)
	if err != nil {
		return false, "", "", "", errors.Wrap(err, "could not list pods")
	}

	if len(pods.Items) == 0 {
		klog.Warning("No pods found")
		return false, "", "", "", nil
	}

	for _, pod := range pods.Items {
		var ready bool
		var readyReason string
		for _, condition := range pod.Status.Conditions {
			if condition.Type == corev1.PodReady {
				if condition.Status == corev1.ConditionTrue {
					ready = true
				} else {
					readyReason = condition.Reason
				}
			}
		}

		if !ready {
			klog.V(4).Infof("Skipping pod %s, not ready (%s)", pod.Name, readyReason)
			continue
		}

		return true, pod.Status.PodIP, pod.Status.HostIP, pod.Spec.NodeName, nil
	}

	return false, "", "", "", nil
}

// currentRoute gets the current gateway IP for the route to a remote subnet.
// The second return value is if a route was found or not, as not finding a route may not be an error.
func currentRoute(remoteCIDR net.IPNet) (net.IP, bool, error) {
	links, err := netlink.LinkList()
	if err != nil {
		panic(err)
	}
	for _, link := range links {
		routes, err := netlink.RouteList(link, 0)
		if err != nil {
			return nil, false, errors.Wrapf(err, "could not list routes for link %s", link.Attrs().Name)
		}
		for _, r := range routes {
			if r.Dst == nil {
				continue
			}
			if r.Dst.String() == remoteCIDR.String() {
				return r.Gw, true, nil
			}
		}
	}

	return nil, false, nil
}
