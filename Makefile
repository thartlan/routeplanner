binary:
	CGO_ENABLED=0 go build -ldflags="-s -w" -o routeplanner .

image: binary
	docker build -t gitlab-registry.cern.ch/thartlan/routeplanner:dev .

push: image
	docker push gitlab-registry.cern.ch/thartlan/routeplanner:dev
