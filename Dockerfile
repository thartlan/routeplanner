FROM gcr.io/distroless/static

ADD routeplanner .

ENTRYPOINT ["./routeplanner"]
