package main

import (
	"flag"
	"net"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/spf13/pflag"

	"k8s.io/klog"
)

var remoteCIDR net.IPNet
var tunnelPodSelector map[string]string
var tunnelPodNamespace string
var recheckFrequency time.Duration

func init() {
	tunnelPodSelector = make(map[string]string)

	pflag.IPNetVar(&remoteCIDR, "remote-cidr", net.IPNet{}, "remote CIDR to route to")
	//pflag.StringVar(&tunnelServiceName, "tunnel-service", "", "name of the service that selects the tunnel pod")
	pflag.StringToStringVar(&tunnelPodSelector, "tunnel-pod-selector", nil, "label selectors for the tunnel pod, i.e app=tunnel,cloud=gke")
	pflag.StringVar(&tunnelPodNamespace, "tunnel-pod-namespace", "kube-system", "namespace to search for the tunnel pod in")
	pflag.DurationVar(&recheckFrequency, "recheck-frequency", time.Second*15, "how often to recheck the route")
}

func main() {
	klog.InitFlags(nil)
	pflag.CommandLine.AddGoFlagSet(flag.CommandLine)
	pflag.Parse()

	if remoteCIDR.IP == nil || remoteCIDR.IP.IsUnspecified() {
		klog.Fatal("Remote CIDR is unspecified")
	}

	if len(tunnelPodSelector) == 0 {
		klog.Fatal("No pod selector specified")
	}

	localNodeName := os.Getenv("NODE_NAME")
	if localNodeName == "" {
		klog.Fatal("NODE_NAME environment variable not set")
	}

	clientset, err := getKubernetesClientset()
	if err != nil {
		klog.Fatalf("Could not create kubernetes client: %v", err)
	}

	ticker := time.NewTicker(recheckFrequency)

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

loop:
	for {
		err := ensureRoute(clientset, localNodeName, remoteCIDR, tunnelPodNamespace, tunnelPodSelector)
		if err != nil {
			klog.Fatalf("Error planning route: %v", err)
		}

		select {
		case <-sigs:
			break loop
		case <-ticker.C:
			continue loop
		}
	}
}
